<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>提货</title>
    <script src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
    <script>
        function check() {
            var flag = true;
            var cardNum = $("#cardNum").val();
            var password = $("#password").val();
            var name = $("#name").val();
            var tel = $("#tel").val();
            var address = $("#address").val();

            if( cardNum == '' || cardNum == null){
                $("#cardNumMsg").html("卡号不能为空！");
                flag = false;
            }else{
                $("#cardNumMsg").html("");
            }

            if( password == '' || password == null){
                $("#passwordMsg").html("卡密不能为空！");
                flag = false;
            }else{
                $("#passwordMsg").html("");
            }

            if( name == '' || name == null){
                $("#nameMsg").html("收货人不能为空！");
                flag = false;
            }else{
                $("#nameMsg").html("");
            }

            if( tel == '' || name == null){
                $("#telMsg").html("电话不能为空！");
                flag = false;
            }else{
                $("#telMsg").html("");
            }

            if( address == '' || name == null){
                $("#addressMsg").html("地址不能为空！");
                flag = false;
            }else{
                $("#addressMsg").html("");
            }

            if(flag == true){
                $("#takeForm").submit();
            }
        }
    </script>
</head>

    <form id="takeForm" action="${pageContext.request.contextPath}/sale/take.do" method="post">
        <table border="0" align="center">
            <tr>
                <td>卡号：</td>
                <td>
                    <input id="cardNum" type="text" name="cardNum">
                    <span id="cardNumMsg"></span>
                </td>
            </tr>
            <tr>
                <td>卡密：</td>
                <td>
                    <input id="password" type="text" name="password">
                    <span id="passwordMsg"></span>
                </td>
            </tr>
            <tr>
                <td>收件人：</td>
                <td>
                    <input id="name" type="text" name="name">
                    <span id="nameMsg"></span>
                </td>
            </tr>
            <tr>
                <td>电话：</td>
                <td>
                    <input id="tel" type="text" name="tel">
                    <span id="telMsg"></span>
                </td>
            </tr>
            <tr>
                <td>地址：</td>
                <td>
                    <input id="address" type="text" name="address">
                    <span id="addressMsg"></span>
                </td>
            </tr>
            <tr>
                <td colspan="2"><input type="button" value="发起提货" onclick="check()"></td>
            </tr>
            <tr>
                <td colspan="2" align="center">${msg}</td>
            </tr>
        </table>
    </form>
<body>

</body>
</html>
