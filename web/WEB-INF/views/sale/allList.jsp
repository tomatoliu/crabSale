<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2020/3/11
  Time: 20:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <title>已销售卡清单</title>
</head>
<body>

    <h3>
        <a href="${pageContext.request.contextPath}/user/index.do">首页</a><br/>
        已售出总计：${saleList.size()}
    </h3>

    <table border="1" cellspacing="0">
        <tr align="center">
            <th width="50px">序号</th>
            <th width="120px">卡号</th>
<%--            <th width="8%">密码</th>--%>
            <th width="100px">规格</th>
            <th width="100px">数量</th>
            <th width="100px">客户</th>
            <th width="100px">价格</th>
            <th width="100px">状态</th>
<%--            <th width="80px">收货人</th>--%>
<%--            <th>收货电话</th>--%>
<%--            <th>收货地址</th>--%>
<%--            <th>物流单号</th>--%>
<%--            <th>提货时间</th>--%>
        </tr>
        <c:forEach items="${saleList}" var="sale" varStatus="s">
            <tr align="center">
                <td>${s.count}</td>
                <td>${sale.cardNum}</td>
<%--                <td>${sale.password}</td>--%>
                <td>${sale.spec}</td>
                <td>${sale.quantity}</td>
                <td>${sale.customer}</td>
                <td>${sale.price}</td>
                <td>
                    <c:if test="${sale.isUsed == 0}">未提货</c:if>
                    <c:if test="${sale.isUsed == 1}">未发货</c:if>
                    <c:if test="${sale.isUsed == 2}">已发货</c:if>
                </td>
<%--                <td>${sale.name}</td>--%>
<%--                <td>${sale.tel}</td>--%>
<%--                <td>${sale.address}</td>--%>
<%--                <td>${sale.waybill}</td>--%>
<%--                <td><fmt:formatDate type="both" value="${sale.createTime}" /></td>--%>
            </tr>
        </c:forEach>

    </table>
</body>
</html>
