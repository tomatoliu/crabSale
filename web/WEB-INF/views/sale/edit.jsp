<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2020/3/11
  Time: 23:08
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>单号填写</title>
</head>
<body>
    <h3>
        <a href="${pageContext.request.contextPath}/user/index.do">首页</a>
    </h3>

    <form action="${pageContext.request.contextPath}/sale/deliver.do" method="post">
        <table border="0" align="center">
            <tr>
                <td colspan="2"><input type="text" name="id" value="${id}" hidden="hidden"></td>
            </tr>
            <tr>
                <td>快递单号：</td>
                <td><input type="text" name="waybill"></td>
            </tr>
            <tr>
                <td colspan="2">
                    <input type="submit" value="保存">
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
