
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>添加已售卡</title>
    <script src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
    <script>
        function save() {
            var flag = true;
            var cardNum = $("#cardNum").val();
            var password = $("#password").val();
            var spec = $("#spec").val();
            var quantity = $("#quantity").val();
            var customer = $("#customer").val();
            var price = $("#price").val();

            if( cardNum == '' || cardNum == null){
                $("#cardNumMsg").html("卡号不能为空！");

                flag = false;
            }else{
                $("#cardNumMsg").html("");
            }

            if( password == '' || password == null){
                $("#passwordMsg").html("卡密不能为空！");
                flag = false;
            }else{
                $("#passwordMsg").html("");
            }

            if( spec == '' || spec == null){
                $("#specMsg").html("规格不能为空！");
                flag = false;
            }else{
                $("#specMsg").html("");
            }

            if( quantity == '' || quantity == null){
                $("#quantityMsg").html("数量不能为空！");
                flag = false;
            }else{
                $("#quantityMsg").html("");
            }

            if( customer == '' || customer == null){
                $("#customerMsg").html("客户不能为空！");
                flag = false;
            }else{
                $("#customerMsg").html("");
            }

            if( price == '' || price == null){
                $("#priceMsg").html("价格不能为空！");
                flag = false;
            }else{
                $("#priceMsg").html("");
            }

            if(flag == true){
                $("#saveForm").submit();
            }


        }
    </script>
</head>
<body>
    <h3><a href="${pageContext.request.contextPath}/user/index.do">首页</a></h3>
    <form id="saveForm" action="${pageContext.request.contextPath}/sale/save.do" method="post">
        <table border="0" align="center">
            <tr>
                <td>卡号：</td>
                <td>
                    <input id="cardNum" type="text" name="cardNum">
                    <span id="cardNumMsg"></span>
                </td>
            </tr>
            <tr>
                <td>卡密：</td>
                <td>
                    <input id="password" type="text" name="password">
                    <span id="passwordMsg"></span>
                </td>
            </tr>
            <tr>
                <td>规格：</td>
                <td>
                    <input id="spec" type="text" name="spec">
                    <span id="specMsg"></span>
                </td>
            </tr>
            <tr>
                <td>数量：</td>
                <td>
                    <input id="quantity" type="text" name="quantity">
                    <span id="quantityMsg"></span>
                </td>
            </tr>
            <tr>
                <td>客户：</td>
                <td>
                    <input id="customer" type="text" name="customer">
                    <span id="customerMsg"></span>
                </td>
            </tr>
            <tr>
                <td>售价：</td>
                <td>
                    <input id="price" type="text" name="price">
                    <span id="priceMsg"></span>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center"><input type="button" value="保存" onclick="save();"></td>
            </tr>
            <tr>
                <td colspan="2">${msg}</td>
            </tr>
        </table>
    </form>

    <form action="${pageContext.request.contextPath}/sale/saveAll.do" method="post" enctype="multipart/form-data">
        <table border="0" align="center">
            <tr>
                <td><input type="file" name="cardsExcel"></td>
                <td><input type="submit" value="批量添加"></td>
            </tr>
        </table>
    </form>
</body>
</html>
