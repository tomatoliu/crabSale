<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2020/3/11
  Time: 15:35
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>首页</title>
</head>
<body>

    <table border="0" align="center" width="80%">
        <tr align="center">
            <td><a href="${pageContext.request.contextPath}/sale/add.do">添加已售卡</a></td>
            <td><a href="${pageContext.request.contextPath}/sale/findAll.do">查看已销售卡清单</a> </td>
            <td><a href="${pageContext.request.contextPath}/sale/findUsed0.do">查看未提货卡清单</a> </td>
            <td><a href="${pageContext.request.contextPath}/sale/findUsed1.do">查看未发货卡清单</a></td>
            <td><a href="${pageContext.request.contextPath}/sale/findUsed2.do">查看已发货卡清单</a></td>
        </tr>
    </table>

</body>
</html>
