package com.lxp.crabsales.service.impl;

import com.lxp.crabsales.mapper.UserMapper;
import com.lxp.crabsales.model.User;
import com.lxp.crabsales.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper mapper;

    @Override
    public User login(User user) {
        User loginUser = mapper.findUserByUser(user);

        return loginUser;
    }
}
