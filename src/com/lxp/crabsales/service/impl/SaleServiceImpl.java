package com.lxp.crabsales.service.impl;

import com.lxp.crabsales.mapper.SaleMapper;
import com.lxp.crabsales.model.Sale;
import com.lxp.crabsales.service.SaleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SaleServiceImpl implements SaleService {
    @Autowired
    private SaleMapper mapper;

    @Override
    public Sale findSaleByCard(Sale sale) {
        return mapper.findSaleByCard(sale);
    }

    @Override
    public Sale findSaleByCardNum(Sale sale) {
        return mapper.findSaleByCardNum(sale.getCardNum());
    }

    @Override
    public int saveOrUpdate(Sale sale) {
        int result;
        if(sale.getId() ==  null){
            result = mapper.insert(sale);

        }else{
            result = mapper.updateByPrimaryKeySelective(sale);
        }
        return result;
    }

    @Override
    public List<Sale> findAll() {
        return mapper.findAll();
    }

    @Override
    public List<Sale> findUsed(int isUsed) {

        return mapper.findSaleByIsUsed(isUsed);
    }
}
