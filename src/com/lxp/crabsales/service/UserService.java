package com.lxp.crabsales.service;

import com.lxp.crabsales.model.User;

public interface UserService {

    User login(User user);
}
