package com.lxp.crabsales.service;

import com.lxp.crabsales.model.Sale;

import java.util.List;

public interface SaleService {

    Sale findSaleByCard(Sale sale);

    Sale findSaleByCardNum(Sale sale);

    int saveOrUpdate(Sale sale);
    List<Sale> findAll();

    List<Sale> findUsed(int isUsed);
}
