package com.lxp.crabsales.mapper;

import com.lxp.crabsales.model.User;

public interface UserMapper {
    User findUserByUser(User user);

}
