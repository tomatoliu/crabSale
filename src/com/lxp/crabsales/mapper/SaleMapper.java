package com.lxp.crabsales.mapper;

import com.lxp.crabsales.model.Sale;

import java.util.List;

public interface SaleMapper {
    Sale findSaleByCard(Sale sale);

    int insert(Sale sale);

    int updateByPrimaryKeySelective(Sale sale);

    Sale findSaleByCardNum(String cardNum);

    List<Sale> findAll();
    List<Sale> findSaleByIsUsed(int isUsed);



}
