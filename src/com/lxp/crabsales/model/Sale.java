package com.lxp.crabsales.model;

import java.util.Date;

public class Sale {
    private Integer id;
    //卡号
    private String cardNum;
    //卡密
    private String password;
    //规格
    private String spec;
    //数量
    private Integer quantity;
    //客户名称
    private String customer;
    //售价
    private Integer price;
    //是否提货（0：未提货；1：已提货未发货；3：已发货）
    private Integer isUsed;
    //收件人姓名
    private String name;
    //收件电话
    private String tel;
    //收件地址
    private String address;
    //快递单号
    private String waybill;
    //发起提货时间
    private Date createTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCardNum() {
        return cardNum;
    }

    public void setCardNum(String cardNum) {
        this.cardNum = cardNum;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSpec() {
        return spec;
    }

    public void setSpec(String spec) {
        this.spec = spec;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public Integer getIsUsed() {
        return isUsed;
    }

    public void setIsUsed(Integer isUsed) {
        this.isUsed = isUsed;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getWaybill() {
        return waybill;
    }

    public void setWaybill(String waybill) {
        this.waybill = waybill;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Sale{" +
                "id=" + id +
                ", cardNum='" + cardNum + '\'' +
                ", password='" + password + '\'' +
                ", spec='" + spec + '\'' +
                ", quantity=" + quantity +
                ", customer='" + customer + '\'' +
                ", price='" + price + '\'' +
                ", isUsed=" + isUsed +
                ", name='" + name + '\'' +
                ", tel='" + tel + '\'' +
                ", address='" + address + '\'' +
                ", waybill='" + waybill + '\'' +
                ", createTime=" + createTime +
                '}';
    }
}
