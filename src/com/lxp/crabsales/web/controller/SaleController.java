package com.lxp.crabsales.web.controller;

import com.lxp.crabsales.model.Sale;
import com.lxp.crabsales.service.SaleService;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("sale")
public class SaleController {

    @Autowired
    private SaleService service;

    @RequestMapping("take")
    public String take(Model model, Sale sale){
        if(sale.getCardNum() != null) {
            Sale saleByCard = service.findSaleByCard(sale);
            if (saleByCard == null) {
                model.addAttribute("msg", "卡号或密码错误，请核对后重试！");
            } else {
                switch (saleByCard.getIsUsed()){
                    case 0 :
                        sale.setId(saleByCard.getId());
                        sale.setIsUsed(1);
                        sale.setCreateTime(new Date());
                        service.saveOrUpdate(sale);
                        model.addAttribute("msg", "提货成功，请耐心等待发货！");
                        break;
                    case 1 :
                        model.addAttribute("msg", "卡已被使用，卡已作废");
                        break;
                    case 2 :
                        model.addAttribute("msg", "已发货，运单号为");
                        break;
                }
            }
        }
        return "sale/take";
    }

    @RequestMapping("add")
    public String add(){
        return "sale/add";
    }

    @RequestMapping("save")
    public String save(Model model, Sale sale){
        if(sale.getCardNum() != null){
            Sale saleByCardNum = service.findSaleByCardNum(sale);
            if(saleByCardNum == null){
                int result = service.saveOrUpdate(sale);
                if(result > 0){
                    model.addAttribute("msg","添加成功");
                }else{
                    model.addAttribute("msg","添加失败");
                }
            }else{
                model.addAttribute("msg","卡号已存在，请勿重复添加");
            }
        }
        return "sale/add";
    }

    @RequestMapping("findAll")
    public String findAll(Model model){
        List<Sale> saleList = service.findAll();
        model.addAttribute("saleList",saleList);
        return "sale/allList";
    }

    @RequestMapping("findUsed0")
    public String findUsed0(Model model){

        List<Sale> saleList = service.findUsed(0);

        model.addAttribute("saleList",saleList);
        return "sale/usedList0";
    }

    @RequestMapping("findUsed1")
    public String findUsed1(Model model){

        List<Sale> saleList = service.findUsed(1);

        model.addAttribute("saleList",saleList);
        return "sale/usedList1";
    }

    @RequestMapping("findUsed2")
    public String findUsed2(Model model){

        List<Sale> saleList = service.findUsed(2);

        model.addAttribute("saleList",saleList);
        return "sale/usedList2";
    }

    @RequestMapping("edit")
    public String edit(Model model, int id){
        model.addAttribute("id",id);
        return "sale/edit";
    }

    @RequestMapping("deliver")
    public String deliver(Sale sale){
        sale.setIsUsed(2);
        System.out.println(sale);
        service.saveOrUpdate(sale);

        return "redirect:findUsed1.do";
    }

    @RequestMapping("saveAll")
    public String saveAll(Model model, MultipartFile cardsExcel){

        String suffix = cardsExcel.getOriginalFilename().split("\\.")[1];
        Workbook excel = null;

        try {
            if("xls".equals(suffix)){
                excel = new HSSFWorkbook(cardsExcel.getInputStream());
            }else if("xlsx".equals(suffix)){
                excel = new XSSFWorkbook(cardsExcel.getInputStream());
            }else{
                model.addAttribute("msg","只能上传excel表格");
                return "sale/add";
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        List<Sale> saleList = new ArrayList<>();
        Sheet sheet = excel.getSheetAt(0);
        for(int i = 1; i < sheet.getLastRowNum(); i++){
            Row row = sheet.getRow(i);
            if(row != null){
                Sale sale = new Sale();
                for(Cell cell : row){
                    cell.setCellType(Cell.CELL_TYPE_STRING);
                }

                sale.setCardNum(row.getCell(0).getStringCellValue());
                sale.setPassword(row.getCell(1).getStringCellValue());
                sale.setSpec(row.getCell(2).getStringCellValue());
                sale.setQuantity(Integer.parseInt(row.getCell(3).getStringCellValue()));
                sale.setCustomer(row.getCell(4).getStringCellValue());
                sale.setPrice(Integer.parseInt(row.getCell(5).getStringCellValue()));
                saleList.add(sale);
            }else{
                model.addAttribute("msg","表格中无数据");
                return "sale/add";
            }
        }
        for(Sale sale : saleList){
            service.saveOrUpdate(sale);
        }
        model.addAttribute("msg","批量添加成功");
        return "sale/add";
    }
}
