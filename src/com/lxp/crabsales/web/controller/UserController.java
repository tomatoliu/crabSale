package com.lxp.crabsales.web.controller;

import com.lxp.crabsales.model.User;
import com.lxp.crabsales.service.UserService;
import com.lxp.crabsales.service.impl.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;


@Controller
@RequestMapping("user")
public class UserController {

    @Autowired
    private UserService service = new UserServiceImpl();

    @RequestMapping("index")
    public String list(){
        return "user/index";
    }

    @RequestMapping("login")
    public String login(User user, Model model, HttpSession session){
        if(user.getUsername() != null && user.getPassword() != null){
            User loginUser = service.login(user);
            if (loginUser == null){
                model.addAttribute("msg","用户名或密码错误，请核对后重试！");
            }else{
                session.setAttribute("loginUser",loginUser);
                return "user/index";
            }
        }
        return "user/login";
    }
}
